## Content

This data repository accompanies the paper
"Verification benchmarks for single-phase flow in three-dimensional fractured porous media"
by Inga Berre, Wietse M Boon, Bernd Flemisch, Alessio Fumagalli, Dennis Gläser, Eirik Keilegavlen, Anna Scotti, Ivar Stefansson, Alexandru Tatomir, Konstantin Brenner, Samuel Burbulla, Philippe Devloo, Omar Duran, Marco Favino, Julian Hennicker, I-Hsien Lee, Konstantin Lipnikov, Roland Masson, Klaus Mosthaf, Maria Giuseppina Chiara Nestola, Chuen-Fa Ni, Kirill Nikitin, Philipp Schädle, Daniil Svyatskiy, Ruslan Yanbarisov, Patrick Zulian.
DOI [10.1016/j.advwatres.2020.103759](https://doi.org/10.1016/j.advwatres.2020.103759).

Four benchmark cases are considered in the following subfolders:

* [single](single):  Refers to Section "4.1 Case 1: single fracture" of the document.

* [regular](regular): Refers to Section "4.2 Case 2: regular fracture network" of the document.

* [small_features](small_features):  Refers to Section "4.3 Case 3: network with small features" of the document.

* [field](field):  Refers to Section "4.4 Case 4: a field case" of the document.

For each benchmark, geometry and result data as well as plotting scripts are provided in corresponding subfolders:

* `geometry`: a geometrical description of the matrix domain and fracture network
in form of a Gmsh input file `gmsh.geo`.

* `results`: usually `csv` files as required by the corresponding case.

* `scripts`: Python scripts to process the result data.

## Compare your results

The Jupyter notebook [compare_my_method](compare_my_method.ipynb) facilitates the
comparison of new results with the published ones.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.iws.uni-stuttgart.de%2Fbenchmarks%2Ffracture-flow-3d/master?filepath=compare_my_method.ipynb)

## Reproduce the figures

For each benchmark case, the corresponding folder contains a Jupyter notebook for
the reproduction of the figures in the publication:
* [single](single/scripts/single.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.iws.uni-stuttgart.de%2Fbenchmarks%2Ffracture-flow-3d/master?filepath=single/scripts/single.ipynb)

* [regular](regular/scripts/regular.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.iws.uni-stuttgart.de%2Fbenchmarks%2Ffracture-flow-3d/master?filepath=regular/scripts/regular.ipynb)

* [small_features](small_features/scripts/small_features.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.iws.uni-stuttgart.de%2Fbenchmarks%2Ffracture-flow-3d/master?filepath=small_features/scripts/small_features.ipynb)

* [field](field/scripts/field.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.iws.uni-stuttgart.de%2Fbenchmarks%2Ffracture-flow-3d/master?filepath=field/scripts/field.ipynb)
