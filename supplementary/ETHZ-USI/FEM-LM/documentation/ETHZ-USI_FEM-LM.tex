\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{authblk}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{filecontents}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{tikz}

\newcommand{\fixme}[1]{ { \bf \color{red}FIX ME \color{black} #1 } }

\title{Lagrange multiplier $L^2$-projection method}

\author[1]{Patrick Zulian}
\author[2]{Philipp Sch\"{a}dle}
\affil[1]{Institute~of~Computational~Science,~USI~Lugano,~Switzerland}
\affil[2]{Geothermal Energy and Geofluids Group, Department of Earth Sciences,~ETH~Z\"urich,~Switzerland}



\date{June 2019}


\begin{document}

\maketitle

\section{Lagrange multiplier method}
Using Lagrange multipliers to model fluid flow through 2D and 3D fractured porous media was first introduced by~\cite{koeppel_2018}. 
The applied Lagrange multipliers enable variable coupling between the fracture and matrix domain. 
Information transfer between the non-conforming mesh domains (i.e.\ the fractures and the matrix mesh) is establishes by variational transfer.
\cite{koeppel_2018} show the uniqueness of the solution for the primal formulation of the continuous flow problem.
In a subsequent work~\cite{koeppel_2019} presented a stabilized formulation which improves conditioning.
All their numerical experiments are conducted on a 2D matrix domain.\\
\cite{schaedle_2019} applied and tested the Lagrange multiplier method in 3D by combining it with a 3D variational transfer operator presented by~\cite{krause_2016}.
They further employed a dual Lagrange multiplier space which significantly improved the conditioning of the arising system of equations both in 2D and 3D.\\
\\
For the purpose of this work the method presented in \cite{schaedle_2019} is extended by advective transport of a solute.
Stabilization of the advection problem is achieved by using an algebraic flux correction.
Generally, the method is based on a hybrid-dimensional problem formulation for flow and transport.
While no relation between the fracture and the matrix mesh is required (see Figure~\ref{fig:meshfig}), the fractures have to coincide at their intersections. 
In the current implementation no additional constraints are defined at the fracture intersections. However, this could be added easily.
%
\subsection{Flow problem}
The variational formulation of the Lagrange multiplier method for fluid flow is given as follows: find $h_3 \in V_3, h_2 \in V_2, \lambda \in \Lambda$
\begin{equation}
\label{eq:1}
\begin{aligned}
(\mathbb{K}_3 \nabla h_3, \nabla  \bar h_3) + (\lambda, \bar h_3)  & = 0 \\
(\epsilon_2\mathbb{K}_2 \nabla h_2, \nabla  \bar h_2) -  (\lambda,  \bar h_2) & = 0 \\
 (h_3 - h_2, \bar \lambda) & = 0,
\end{aligned}
\end{equation}
for all test functions $\bar h_3 \in V_3, \bar h_2 \in V_2,\bar \lambda \in \Lambda$,
where $(\cdot, \cdot)$ denotes the $L^2$ inner product and $V_2, V_3, \Lambda$ suitable function spaces. 
$\Lambda$ is the space of Lagrange multipliers and the subscript denotes the dimension of the domain.
The permeability $\mathbb{K}_d$ is converted from $K_d$ such that $\mathbb{K}_2  = K_2^\text{eq} \mathbf{I}$,  $\mathbb{K}_3  = K_3^\text{eq} \mathbf{I}$, $ K_d^\text{eq} =  K_d/\epsilon_d$, and $\epsilon_3 = 1$.\\
%
\subsection{Transport problem}
The variational formulation of the continuous Lagrange multiplier problem for advective transport is given as follows: find $c_3 \in V_3, c_2 \in V_2, \mu \in \Lambda$ 
\begin{equation}
\label{eq:3}
\begin{aligned}
(\phi_3 \partial_t c_3, \bar c_3) + (\nabla c_3 \cdot \mathbf{u}_3, \bar c_3)   + (\mu, \bar c_3) & = 0 \\
(\epsilon_2\phi_2 \partial_t c_2, \bar c_2) + (\nabla c_2 \cdot \mathbf{u}_2, \bar c_2)  - (\mu, \bar c_2) & = 0  \\
 (c_3 - c_2, \bar \mu) & = 0,
\end{aligned}
\end{equation}
for all $\bar c_3 \in V_3, \bar c_2 \in V_2,\bar \mu \in \Lambda$. %,
%
In order to prevent oscillation of the transport and to stabilize the problem an algebraic flux correction following \citep{kuzmin_2012,turek1999efficient} is applied to the aggregate spatial differential operator.
%
\subsection{Discretization}
The discrete counterparts of the variational formulations in Eqs.~\eqref{eq:1} and ~\eqref{eq:3} are formulated in a finite element framework.
For this formulations three distinct meshes are defined for matrix ($\mathcal{T}_\Omega$), fracture ($\mathcal{T}_\gamma$), and Lagrange multiplier ($\mathcal{T}_\lambda$) with the respective mesh widths $h_{\Omega}$, $h_{\gamma}$, and $h_{\lambda}$.
The meshes can be generated with a wide range of geometrically affine elements and the order of the elements might be zero, first or second.
However, for this work $\mathbb{P}^1$ elements are chosen for all domains.
Different choices of Lagrange multiplier space are available and are discussed in ~\cite{schaedle_2019},~\cite{koeppel_2018}, and~\cite{koeppel_2019}.
Generally, in order to prevent a poorly conditioned system matrix, it is necessary to satisfy $h_{\lambda} \geq \max(h_{\Omega}, h_{\gamma})$.
%
\subsection{Volume--surface information transfer}
%
Information transfer between the mutually non-conforming discretizations of the problem is obtained by $L^2$-projection.
Here, intersections between the matrix mesh and the fracture mesh are computed and meshed in order to perform quadrature for the coupling terms in Eqs.~\eqref{eq:1} and~\eqref{eq:3}.
The applied algorithms are fully automated and no prior knowledge about the relation of the meshes is required. 
Further details regarding the information transfer procedure can be found in~\cite{schaedle_2019} and~\cite{krause_2016}.
%
%%%
% MESH FIGURE WITH NON-CONFORMING FRACTURE AND MATRIX MESHES
%%%
\begin{figure}[h]
    \centering
    \includegraphics[width=.6\linewidth]{fracture_matrix_mesh.png}
    \caption{Matrix mesh (wireframe) and fracture domain (green) with fracture mesh (red).}
    \label{fig:meshfig}
\end{figure}
%
\section{Implementation}
All routines are implemented within the open-source software library \emph{Utopia}~\cite{utopiagit}. Besides state-of-the-art libraries for finite element discretization and linear algebra calculations (i.e.\ \emph{libMesh}~\cite{libmesh}, \emph{PETSc}~\cite{petsc}), \emph{Utopia} uses \emph{MOONoLith}~\cite{moonolith} for the intersection detection.
The algebraic linear systems of Eqs.~\eqref{eq:1} and~\eqref{eq:3} is solved with the \emph{MUMPS}~\cite{mumps} direct solver.


\bibliographystyle{plainnat}
\bibliography{references} 

\end{document}
